import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../auth.service';

/**
 * Component responsible for logging out the user.
 */
@Component({
  template: '', // Empty template as this component doesn't have any UI
})
export class LogoutComponent {
  constructor(private router: Router, private cookieService: CookieService, private authService: AuthService) {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
