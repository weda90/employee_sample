import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { config } from './config';

@Injectable({
  providedIn: 'root'
})
/**
 * Service responsible for handling authentication-related functionality.
 */
export class AuthService {

  constructor(private cookieService: CookieService) {}

  /**
   * Checks if the user is logged in by checking if the username cookie exists.
   * @returns A boolean indicating whether the user is logged in or not.
   */
  public isLogged(): boolean {
    const username = this.cookieService.get('username');
    return !!username;
  }

  /**
   * Checks if the provided credentials match the configured username and password.
   * @param username - The username to check.
   * @param password - The password to check.
   * @returns A boolean indicating whether the credentials are valid or not.
   */
  public checkCredentials(username: string | undefined, password: string | undefined): boolean {
    return username === config.username && password === config.password;
  }

  /**
   * Logs in the user by setting the username cookie.
   * @param username - The username to set as the logged-in user.
   */
  public login(username: string): void {
    this.cookieService.set('username', username);
  }

  /**
   * Logs out the user by deleting all cookies.
   */
  public logout(): void {
    this.cookieService.deleteAll();
  }
}
