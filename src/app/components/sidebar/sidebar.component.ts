import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  logoUrl: SafeResourceUrl | undefined;
  
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.logoUrl = this.sanitizer.bypassSecurityTrustResourceUrl('assets/images/example_logo.svg');
  }
  
}
