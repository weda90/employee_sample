import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router, // Dependency injection for the Router service
    private authService: AuthService // Dependency injection for the AuthService
  ) {}

  canActivate(): boolean {
    const isLogged = this.authService.isLogged(); // Check if the user is logged in

    if (!isLogged) { // If the user is not logged in
      this.router.navigate(['/login']); // Navigate to the login page
    }

    return isLogged; // Return whether the user is logged in or not
  }
}


