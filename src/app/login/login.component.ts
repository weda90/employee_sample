import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service'
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  // Declare username and password with a union type that includes undefined
  username: string | undefined;
  password: string | undefined;
  
  constructor(private router: Router, private cookieService: CookieService, private authService: AuthService) { }

  ngOnInit(): void {
    // Check if user is already logged in
    if (this.authService.isLogged()) {
      this.router.navigate(['/']); // Redirect to home route if logged in
    }
  }
  
  login() {
    const isValid = this.authService.checkCredentials(this.username, this.password);
    
    // Check if credentials are valid
    if (isValid) {
      // Check if username is provided
      if (this.username) {
        this.authService.login(this.username); // Login user by saving username
        this.router.navigate(['/']); // Redirect to home route after successful login
      } else {
        console.log('Invalid username. Unable to save username.');
        // Log an error message when username is not provided
      }
    } else {
      console.log('Invalid credentials. Unable to save username.');
      // Log an error message when credentials are invalid
    }
  }
  
  

  

}
