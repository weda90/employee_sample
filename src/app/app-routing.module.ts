import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';
import { EmployeeComponent } from './employee/employee.component';
import { MainComponent } from './main/main.component';

// Route configurations for the application
const routes: Routes = [
  { path: 'login', component: LoginComponent }, // Route for the login page, uses LoginComponent
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] }, // Route for the logout page, uses LogoutComponent and requires authentication with AuthGuard
  { path: '', component: MainComponent, canActivate: [AuthGuard], children: [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'employee',
      component: EmployeeComponent
    }
  ] }, // Route for the root path, requires authentication with AuthGuard and has no child routes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)], // Configure the routes using RouterModule.forRoot()
  exports: [RouterModule] // Export the configured routes module
})
export class AppRoutingModule { }

