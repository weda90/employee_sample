import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
// Define the Employee interface to specify the properties of an employee
interface Employee {
  username: string; // The username of the employee
  firstName: string; // The first name of the employee
  lastName: string; // The last name of the employee
  email: string; // The email of the employee
  birthDate: Date; // The birth date of the employee
  basicSalary: number; // The basic salary of the employee
  status: string; // The status of the employee
  group: string; // The group of the employee
  description: Date; // The description of the employee
}

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit, OnDestroy {

  // Declare component properties
  username: any; // The username of the current employee
  firstName: any; // The first name of the current employee
  lastName: any; // The last name of the current employee
  email: any; // The email of the current employee
  birthDate: any; // The birth date of the current employee
  basicSalary: any; // The basic salary of the current employee
  status: any; // The status of the current employee
  group: any; // The group of the current employee
  description: any; // The description of the current employee
  userForm: any;

  private _jsonURL = 'assets/sample.json'; // The URL of the JSON file containing employee data

  dtOptions: DataTables.Settings = {}; // The options for the DataTables library
  employees: Employee[] = []; // The array of employees
  dtTrigger: Subject<any> = new Subject<any>(); // The event trigger for DataTables

  modalRef!: NgbModalRef; 
  @ViewChild('EmployeeModal')
  templateRef!: TemplateRef<any>; // The reference to the modal template

  departments = [
    "Sales",
    "Marketing",
    "Finance",
    "Human Resources",
    "IT",
    "Operations",
    "Customer Service",
    "Research and Development",
    "Legal",
    "Administration"
  ]; // The list of departments

  disable: boolean = false; // Flag indicating whether input fields should be disabled or not

  constructor(private http: HttpClient, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 2
    }; // Set the options for DataTables

    this.http.get<any>(this._jsonURL)
      .subscribe(data => {
        this.employees = data; // Set the employees array with the data from the JSON file
        
        // Calling the DT trigger to manually render the table
        this.dtTrigger.next(this.employees);
      });
  }
  
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  // Open the modal dialog
  open(content: any) {
    this.modalService.open(content);
  }

  // Get the current date in the format 'YYYY-MM-DD'
  getCurrentDate(): string {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = ('0' + (currentDate.getMonth() + 1)).slice(-2);
    const day = ('0' + currentDate.getDate()).slice(-2);
    return `${year}-${month}-${day}`;
  }

  // Check if the email is valid
  emailIsValid(): boolean {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailPattern.test(this.email);
  }

  // Save the employee
  saveEmployee() {
    // Call SweetAlert to display a confirmation dialog
    Swal.fire({
     title: 'Confirm Add Employee',
     text: 'Are you sure you want to add this employee?',
     icon: 'question',
     showCancelButton: true,
     confirmButtonText: 'Yes, add it!',
     cancelButtonText: 'No, cancel!',
   }).then((result) => {
     if (result.value) {
       // Implement your logic to add an employee here

       Swal.fire(
         'Added!',
         'The employee has been added.',
         'success'
       );
     } else if (result.dismiss === Swal.DismissReason.cancel) {
       Swal.fire(
         'Cancelled',
         'The employee was not added.',
         'error'
       );
     }
   });
 }

  // Show the details of an employee
  showEmployee(employee: any) {
    this.disable = true;
    // Assign the values of the employee to component properties
    this.username = employee.username;
    this.firstName = employee.firstName;
    this.lastName = employee.lastName;
    this.email = employee.email;
    this.birthDate = employee.birthDate;
    this.basicSalary = employee.basicSalary;
    this.status = employee.status;
    this.group = employee.group;
    this.description = employee.description;
    this.userForm = employee.userForm;

    this.modalRef = this.modalService.open(this.templateRef);
  }

  // Add a new employee
  addEmployee() {
    this.disable = false;
    this.username = '';
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.birthDate = '';
    this.basicSalary = '';
    this.status = '';
    this.group = '';
    this.description = '';
    this.userForm = '';
   this.modalRef = this.modalService.open(this.templateRef);
  }

  // Edit an existing employee
  editEmployee(employee: any) {
    this.disable = false;

    // Assign the values of the employee to component properties
    this.username = employee.username;
    this.firstName = employee.firstName;
    this.lastName = employee.lastName;
    this.email = employee.email;
    this.birthDate = employee.birthDate;
    this.basicSalary = employee.basicSalary;
    this.status = employee.status;
    this.group = employee.group;
    this.description = employee.description;
    this.userForm = employee.userForm;

    this.modalRef = this.modalService.open(this.templateRef);
  }

  // Delete an employee
  deleteEmployee(employee: any) {
    // Handle the delete action for the employee object
    console.log('Delete employee:', employee);
    // Add your custom delete logic here
    // Call SweetAlert to display a success message
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
    }).then((result) => {
      if (result.value) {
        // Delete logic here
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your data is safe :)',
          'error'
        );
      }
    });
    
  }
}
