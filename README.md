# EmployeeSample

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.1.

# Running the Application

This guide will walk you through the steps to run the Angular application locally on your machine. Before starting, ensure that you have the Angular CLI installed and have performed the necessary package installations using npm.

## Prerequisites

- Node.js and npm: Make sure you have Node.js and npm (Node Package Manager) installed on your system. You can download and install them from the official Node.js website: [https://nodejs.org ↗](https://nodejs.org)

- Angular CLI: Install the Angular CLI globally on your machine by running the following command in your terminal or command prompt:

  `````bash
  npm install -g @angular/cli
  ```

## Installation

1. Clone the repository or download the source code for the Angular application.

2. Open a terminal or command prompt and navigate to the root directory of the project.

3. Run the following command to install the project dependencies specified in the `package.json` file:

   ````bash
   npm install
   ```

   This command will download and install all the required packages and dependencies.

## Running the Application

1. After the installation is complete, run the following command to start the Angular development server:

   ````bash
   ng serve
   ```

   This command will compile the application and start the development server.

2. Open your preferred web browser and navigate to `http://localhost:4200`. The Angular application will be loaded, and you can interact with it in the browser.

   If port 4200 is already in use on your machine, you can specify a different port by running the following command instead:

   ````bash
   ng serve --port <port_number>
   ```

   Replace `<port_number>` with the desired port, e.g., `ng serve --port 5000`.

3. Any changes you make to the source code will trigger an automatic rebuild, and the browser will refresh to reflect the changes. You can continue developing and testing the application in this manner.

## Conclusion

You have successfully set up and run the Angular application locally on your machine. You can now explore the features and functionalities of the application in your web browser.

If you encounter any issues during the installation or running process, please refer to the project documentation or seek assistance from the development team.

Happy coding!